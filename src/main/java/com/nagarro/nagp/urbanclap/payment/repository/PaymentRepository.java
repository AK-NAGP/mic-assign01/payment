package com.nagarro.nagp.urbanclap.payment.repository;

import com.nagarro.nagp.urbanclap.payment.domain.entity.PaymentEntity;
import com.nagarro.nagp.urbanclap.payment.domain.enums.PaymentStatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PaymentRepository extends JpaRepository<PaymentEntity, Integer> {

    List<PaymentEntity> findAllByPaymentIdOrderByCreatedOnDesc(UUID paymentId);

    Optional<PaymentEntity> findOneByPaymentIdAndPaymentStatusOrderByCreatedOnDesc(UUID paymentId, PaymentStatusEnum paymentStatus);

    Optional<PaymentEntity> findByPaymentIdAndPaymentStatus(UUID paymentId, PaymentStatusEnum paymentStatus);

}
