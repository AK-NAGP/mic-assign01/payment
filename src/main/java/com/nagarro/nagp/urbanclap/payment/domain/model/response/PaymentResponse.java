package com.nagarro.nagp.urbanclap.payment.domain.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.nagarro.nagp.urbanclap.payment.domain.enums.PaymentStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentResponse extends BaseResponse {

    private Integer id;

    private UUID paymentId;

    private Double charges;

    private Date paymentDate;

    private String serviceName;

    private String paymentMode;

    private String paymentVendor;

    private String paymentVendorId;

    private PaymentStatusEnum paymentStatus;

    public PaymentResponse(String status, String message) {
        super(status, message);
    }
}
