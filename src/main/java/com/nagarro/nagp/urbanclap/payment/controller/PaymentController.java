package com.nagarro.nagp.urbanclap.payment.controller;

import com.nagarro.nagp.urbanclap.payment.domain.model.request.PaymentRequest;
import com.nagarro.nagp.urbanclap.payment.domain.model.response.PaymentResponse;
import com.nagarro.nagp.urbanclap.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;

    @PostMapping
    public ResponseEntity<PaymentResponse> acceptRejectCallbackPayment(
            @RequestBody PaymentRequest paymentRequest
    ) {
        return ResponseEntity.ok(paymentService.acceptPayment(paymentRequest));
    }

    @PostMapping("fetch")
    public ResponseEntity<List<PaymentResponse>> getPaymentDetails(
            @RequestBody PaymentRequest paymentRequest
    ) {
        return ResponseEntity.ok(paymentService.fetchPayments(paymentRequest));
    }

}
