package com.nagarro.nagp.urbanclap.payment.domain.model.request;

import lombok.Data;

import java.util.UUID;

@Data
public class PaymentCreateJmsRequest {

    private UUID paymentId;

    private String paymentMode;

    private String orderId;

    private Double charges;

    private String serviceName;
}
