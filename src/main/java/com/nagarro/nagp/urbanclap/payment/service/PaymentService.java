package com.nagarro.nagp.urbanclap.payment.service;

import com.nagarro.nagp.urbanclap.payment.domain.model.request.PaymentCreateJmsRequest;
import com.nagarro.nagp.urbanclap.payment.domain.model.request.PaymentRequest;
import com.nagarro.nagp.urbanclap.payment.domain.model.response.PaymentResponse;

import java.util.List;

public interface PaymentService {

    PaymentResponse acceptPayment(PaymentRequest paymentRequest);

    void createPayment(PaymentCreateJmsRequest paymentCreateJmsRequest);

    List<PaymentResponse> fetchPayments(PaymentRequest paymentRequest);
}
