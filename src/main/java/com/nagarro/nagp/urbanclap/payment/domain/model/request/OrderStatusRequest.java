package com.nagarro.nagp.urbanclap.payment.domain.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderStatusRequest {

    private String orderId;

    private String orderStatus;

}
