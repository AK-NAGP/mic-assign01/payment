package com.nagarro.nagp.urbanclap.payment.service.impl;

import com.nagarro.nagp.urbanclap.payment.domain.entity.PaymentEntity;
import com.nagarro.nagp.urbanclap.payment.domain.enums.PaymentStatusEnum;
import com.nagarro.nagp.urbanclap.payment.domain.model.request.OrderStatusRequest;
import com.nagarro.nagp.urbanclap.payment.domain.model.request.PaymentCreateJmsRequest;
import com.nagarro.nagp.urbanclap.payment.domain.model.request.PaymentRequest;
import com.nagarro.nagp.urbanclap.payment.domain.model.response.PaymentResponse;
import com.nagarro.nagp.urbanclap.payment.jms.producer.OrderStatusProducer;
import com.nagarro.nagp.urbanclap.payment.repository.PaymentRepository;
import com.nagarro.nagp.urbanclap.payment.service.PaymentService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentRepository paymentRepository;

    @Autowired
    private OrderStatusProducer orderStatusProducer;

    @Override
    public PaymentResponse acceptPayment(PaymentRequest paymentRequest) {
        Optional<PaymentEntity> paymentEntityOptional = paymentRepository.findByPaymentIdAndPaymentStatus(paymentRequest.getPaymentId(), PaymentStatusEnum.SUCCESS);

        if (! paymentEntityOptional.isPresent()) {
            Optional<PaymentEntity> paymentEntityOptional1 = paymentRepository.findOneByPaymentIdAndPaymentStatusOrderByCreatedOnDesc(paymentRequest.getPaymentId(), PaymentStatusEnum.IN_PROGRESS);

            PaymentEntity paymentEntity;
            if (paymentEntityOptional1.isPresent()) {
                paymentEntity = paymentEntityOptional1.get();
                BeanUtils.copyProperties(paymentRequest, paymentEntity);

                //Send Success Payment Status...
                if (paymentRequest.getPaymentStatus().equals(PaymentStatusEnum.SUCCESS)) {
                    CompletableFuture.runAsync(() -> updateOrderStatusSuccess(paymentEntity.getOrderId()));
                }
            } else {

                paymentEntity = new PaymentEntity();
                BeanUtils.copyProperties(paymentRequest, paymentEntity);
                paymentEntity.setCreatedOn(new Date());

            }

            paymentEntity.setPaymentDate(new Date());
            paymentRepository.save(paymentEntity);
            return mapEntityToModel(paymentEntity);

        } else {
            PaymentEntity paymentEntity = paymentEntityOptional.get();
            PaymentResponse paymentResponse = mapEntityToModel(paymentEntity);

            paymentResponse.setStatus("failure");
            paymentResponse.setMessage("Payment already done");

            return paymentResponse;
        }
    }

    private PaymentResponse mapEntityToModel(PaymentEntity paymentEntity) {
        PaymentResponse paymentResponse = new PaymentResponse();
        BeanUtils.copyProperties(paymentEntity, paymentResponse);
        paymentResponse.setPaymentVendor(paymentEntity.getExternalVendor());
        paymentResponse.setPaymentVendorId(paymentEntity.getVendorUniqueId());
        return paymentResponse;
    }

    @Override
    public void createPayment(PaymentCreateJmsRequest paymentCreateJmsRequest) {
        Optional<PaymentEntity> paymentEntityOptional = paymentRepository.findByPaymentIdAndPaymentStatus(paymentCreateJmsRequest.getPaymentId(), PaymentStatusEnum.SUCCESS);

        PaymentEntity paymentEntity;
        if (! paymentEntityOptional.isPresent()) {
            paymentEntity = new PaymentEntity();
            BeanUtils.copyProperties(paymentCreateJmsRequest, paymentEntity);
            paymentEntity.setPaymentStatus(PaymentStatusEnum.IN_PROGRESS);
            paymentEntity.setCreatedOn(new Date());
        } else {
            paymentEntity = paymentEntityOptional.get();
            paymentEntity.setServiceName(paymentCreateJmsRequest.getServiceName());
            paymentEntity.setOrderId(paymentCreateJmsRequest.getOrderId());
            paymentEntity.setPaymentMode(paymentCreateJmsRequest.getPaymentMode());

            //Send Success Payment Status...
            CompletableFuture.runAsync(() -> updateOrderStatusSuccess(paymentEntity.getOrderId()));
        }
        paymentRepository.save(paymentEntity);
    }

    @Override
    public List<PaymentResponse> fetchPayments(PaymentRequest paymentRequest) {
        return paymentRepository
                .findAllByPaymentIdOrderByCreatedOnDesc(paymentRequest.getPaymentId())
                .stream()
                .map(this::mapEntityToModel)
                .collect(Collectors.toList());
    }

    private void updateOrderStatusSuccess(String orderId) {
        orderStatusProducer.sendOrderStatusUpdate(new OrderStatusRequest(orderId, "PAYMENT_DONE"));
    }
}
