package com.nagarro.nagp.urbanclap.payment.domain.enums;

public enum PaymentStatusEnum {
    SUCCESS,
    FAILED,
    IN_PROGRESS
}
