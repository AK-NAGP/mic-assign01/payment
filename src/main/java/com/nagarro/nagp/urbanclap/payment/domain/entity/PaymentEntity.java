package com.nagarro.nagp.urbanclap.payment.domain.entity;

import com.nagarro.nagp.urbanclap.payment.domain.enums.PaymentStatusEnum;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.UUID;

@Entity(name = "payment")
@Data
public class PaymentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private UUID paymentId;

    @Column
    private String orderId;

    @Column
    private String paymentMode;

    @Column(nullable = false)
    private Double charges;

    @Column
    @Enumerated(EnumType.STRING)
    private PaymentStatusEnum paymentStatus;

    @Column
    private String serviceName;

    @Column
    private String externalVendor;

    @Column
    private String vendorUniqueId;

    @Column
    private String vendorComments;

    @Column
    private Date paymentDate;

    @Column(nullable = false)
    private Date createdOn;
}
