package com.nagarro.nagp.urbanclap.payment.jms.producer;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.payment.domain.model.request.OrderStatusRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class OrderStatusProducer {

    private static final String JMS_ORDER_Q = "uc.orders.updateOrderStatus";

    @Autowired
    private Gson gson;

    @Autowired
    private JmsTemplate jmsTemplate;

    public void sendOrderStatusUpdate(OrderStatusRequest orderStatusRequest) {
        String jsonData = gson.toJson(orderStatusRequest);
        jmsTemplate.convertAndSend(JMS_ORDER_Q, jsonData);
    }

}
