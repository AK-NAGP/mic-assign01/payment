package com.nagarro.nagp.urbanclap.payment.domain.model.request;

import com.nagarro.nagp.urbanclap.payment.domain.enums.PaymentStatusEnum;
import lombok.Data;

import java.util.UUID;

@Data
public class PaymentRequest {

    private UUID paymentId;

    private String externalVendor;

    private String vendorUniqueId;

    private String vendorComments;

    private PaymentStatusEnum paymentStatus;

    private Double charges;
}
