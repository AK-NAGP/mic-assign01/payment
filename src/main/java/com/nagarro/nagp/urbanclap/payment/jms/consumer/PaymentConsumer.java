package com.nagarro.nagp.urbanclap.payment.jms.consumer;

import com.google.gson.Gson;
import com.nagarro.nagp.urbanclap.payment.domain.model.request.PaymentCreateJmsRequest;
import com.nagarro.nagp.urbanclap.payment.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class PaymentConsumer {

    @Autowired
    private Gson gson;

    @Autowired
    private PaymentService paymentService;

    @JmsListener(destination = "uc.payments.addPaymentEntity")
    public void paymentEntityListener(String message) {
        PaymentCreateJmsRequest paymentCreateJmsRequest = gson.fromJson(message, PaymentCreateJmsRequest.class);
        paymentService.createPayment(paymentCreateJmsRequest);
    }
    
}
